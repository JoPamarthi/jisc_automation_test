package utils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CommonUtils {
	
	public static void click(final WebDriver driver , By Locator)	
	{
		driver.findElement(Locator).click();
	}
	
	public static void text(final WebDriver driver , By Locator, String text)
	{
		driver.findElement(Locator).sendKeys(text);
	}
	
	public static void dropdown(final WebDriver driver, By Locator, String text)
	{
		new Select(driver.findElement(Locator)).selectByVisibleText(text);
	}
	
	public static void thread(final WebDriver driver, int i) throws InterruptedException
	{
		Thread.sleep(i);
	}
	
	public static void implicit(final WebDriver driver, int i)
	{
		driver.manage().timeouts().implicitlyWait(i, TimeUnit.SECONDS);
	}
	
	public static void ExplicitWaitText(final WebDriver driver, By Locator, String text, int i)
	{
		try {
		WebDriverWait wait = new WebDriverWait(driver , i);
		wait.until(ExpectedConditions.visibilityOfElementLocated(Locator)).sendKeys(text);
		}catch(Exception e) {
			driver.findElement(Locator);
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			 jse.executeScript("arguments[0].value='enter the value here';", driver.findElement(Locator));
		}
	}
	
	public static void ExplicitWaitClick(final WebDriver driver, By Locator, int i)
	{
		WebDriverWait waitclick = new WebDriverWait(driver , i);
		waitclick.until(ExpectedConditions.visibilityOfElementLocated(Locator)).click();
	}
	
	public static void clear(final WebDriver driver, By Locator)
	{
		driver.findElement(Locator).clear();
	}
	
	public static WebElement web (final WebDriver driver, By Locator)
	{
		return driver.findElement(Locator);
	}
}

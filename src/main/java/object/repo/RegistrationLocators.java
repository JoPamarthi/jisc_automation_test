package object.repo;

import org.openqa.selenium.By;

public class RegistrationLocators {
	
	public static By register = By.partialLinkText("Register");
	public static By usersName = By.id("name");
	public static By usersEmail = By.id("email");
	public static By usersWebsite = By.id("url");
	public static By Interests = By.id("jobInterests");
	public static By usersPassword = By.id("password");
	public static By usersConfirmpwd = By.id("confirmPassword");
	public static By regButton = By.xpath("/html/body/div/div/div/form/button");
	public static By pageHeader = By.className("page-header");
	public static By errormsg = By.xpath("/html/body/div/div/div/form/div[1]/p");
	public static By jobSearch = By.cssSelector("body > div > div > div > div.row > div.col-md-7 > h3");
}

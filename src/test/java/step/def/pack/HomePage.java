package step.def.pack;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import object.repo.RegistrationLocators;
import utils.ApplicationConstants;
import utils.BrowserFactory;
import utils.CommonUtils;

public class HomePage extends BrowserFactory {
	
	private WebDriver driver;
	
	@Before("@HomePage")
	public void setup()
	{
		driver = BrowserFactory.getDriver();
		driver.manage().window().maximize();
	}
	
	@Given("^I will navigate to HomePage$")
	public void i_will_navigate_to_HomePage() throws Throwable
	{
	    driver.navigate().to(ApplicationConstants.homepageURL);
	    CommonUtils.thread(driver, 2000);
	}

	@When("^I click on register button$")
	public void i_click_on_register_button() throws Throwable 
	{
	    CommonUtils.click(driver, RegistrationLocators.register);
//	    CommonUtils.thread(driver, 2000);
	}

	@Then("^I can see registration page displayed$")
	public void i_can_see_registration_page_displayed() throws Throwable
	{
	    String pageTitle = driver.getTitle();
	    System.out.println(pageTitle);
	    Assert.assertEquals(pageTitle, "Registration Test");
	}
	
	@After("@HomePage")
	public void teardown()
	{
		driver.close();
	}
	

}

package step.def.pack;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import object.repo.RegistrationLocators;
import utils.ApplicationConstants;
import utils.BrowserFactory;
import utils.CommonUtils;

public class RegistrationPage {
	
	private WebDriver driver;
	
	@Before("@RegistrationPage")
	public void setup()
	{
		driver = BrowserFactory.getDriver();
		driver.manage().window().maximize();
	}
	
	@Given("^I will navigate to registration page$")
	public void i_will_navigate_to_registration_page() throws Throwable 
	{
		
		driver.navigate().to(ApplicationConstants.homepageURL);
		CommonUtils.click(driver, RegistrationLocators.register);
		CommonUtils.thread(driver, 2000);
	}

	@When("^I enter \"([^\"]*)\" \"([^\"]*)\" \"([^\\\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void i_enter(String usersName, String usersEmail, String userWeb, String passWord, String confirmPassword) throws Throwable 
	{
	    CommonUtils.ExplicitWaitText(driver, RegistrationLocators.usersName, usersName, 5);
	    CommonUtils.ExplicitWaitText(driver, RegistrationLocators.usersEmail, usersEmail, 5);
	    CommonUtils.ExplicitWaitText(driver, RegistrationLocators.usersWebsite, userWeb, 5);
	    CommonUtils.ExplicitWaitText(driver, RegistrationLocators.usersPassword, passWord, 5);
	    CommonUtils.ExplicitWaitText(driver, RegistrationLocators.usersConfirmpwd, confirmPassword, 5);
	    //CommonUtils.ExplicitWaitText(driver, RegistrationLocators.Interests, interests, 5);
	}

	@Then("^I will click on Register$")
	public void i_will_click_on_Register() throws Throwable 
	{
		CommonUtils.click(driver, RegistrationLocators.regButton);
	}

	@Then("^I will navigate to dashboard page$")
	public void i_will_navigate_to_dashboard_page() throws Throwable 
	{
		
	     String pageHeader =	driver.findElement(RegistrationLocators.pageHeader).getText();
	     Assert.assertEquals(pageHeader, "Dashboard");
	     System.out.println(pageHeader);
//	     CommonUtils.thread(driver, 2000);
	     
//	       Error Message -- Automation as Interests option not available 
//	       String errorMessage =	driver.findElement(RegistrationLocators.errormsg).getText();
//		   Assert.assertEquals(errorMessage, "You must provide at least one job interest.");
//		   System.out.println(errorMessage);
	}
	
	@After("@RegistrationPage")
	public void teardown()
	{
		driver.close();
	}

}

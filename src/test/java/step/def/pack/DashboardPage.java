package step.def.pack;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import object.repo.RegistrationLocators;
import utils.ApplicationConstants;
import utils.BrowserFactory;
import utils.CommonUtils;

public class DashboardPage {
	
	private WebDriver driver;
	
	@Before("@DashboardPage")
	public void setup()
	{
		driver = BrowserFactory.getDriver();
		driver.manage().window().maximize();
	}
	
	@Given("^I will navigate to the dashboard page$")
	public void i_will_navigate_to_the_dashboard_page() throws Throwable 
	{
	    driver.navigate().to(ApplicationConstants.dashboardURL);
	}

	@When("^I search for top jobs$")
	public void i_search_for_top_jobs() throws Throwable 
	{
		   String jobSearch =	driver.findElement(RegistrationLocators.jobSearch).getText();
		   Assert.assertEquals(jobSearch, "Here are our latest 5 jobs for you…");
		   System.out.println(jobSearch);
	       CommonUtils.thread(driver, 2000);
	}

	@Then("^I will see top five jobs displayed$")
	public void i_will_see_top_five_jobs_displayed() throws Throwable 
	{
		try
		{
	    for (int i=1; 1<=4;i++)
	    {
	    	String contents = driver.findElement(By.cssSelector("body > div > div > div > table > thead > tr > th:nth-child(" + i + ")")).getText();
	    	System.out.println(contents);
//	    	CommonUtils.thread(driver, 2000);
	    }
		} 
		catch (Exception e)
		{
			
		}
	}
	
	@After("@DashboardPage")
	public void teardown()
	{
		driver.close();
	}

}

# JISC Automation Task Two -- End to End Testing using Cucumber Framework.

* Download zip Project from reporsitory.

**Opening this project**

**Eclipse**

* Open Eclipse.
* Click File > Import.
* Type Maven in the search box under Select an import source:
* Select Existing Maven Projects.
* Click Next.
* Click Browse and select the folder that is the root of the Maven project (probably contains the pom.xml file).
* Click Next.
* Click Finish.

## Dependencies:

* Selenium-Java latest maven dependency --> 3.141.59 version.
* JUnit latest maven dependency --> 4.11 version.

## URL:

* Open Jisc_Automation_Test_Analyst project.
* Navigate to src/main/java folder
* Navigate to utils package
* Open ApplicationConstants.java file
* Enter the local reporsitory of the html files as 'url' for both homepageURL and dashboardURL
* Then start running the test cases.

## Automation Task Execution using TestRunner.class

* Open Jisc_Automation_Test_Analyst project.
* Navigate to src/main/java
* Navigate to package --> runner.pack
* Open Class --> TestRunner.java
* Run the class using @RegistrationTest annotation.
* Task will run automatically with mentioned test data followed by DATA VALIDATIONS
* Navigate to Target -> reports. Reports are generated.

## Automation Task Execution using feature files

* Open Jisc_Automation_Test_Analyst project.
* Open Features folder
* Open each individual feature files (Homepage, RegistrationPage, DashboardPage)
* Please only run individually using each feature file.
* I have mentioned tags, so you can execute in group or individually. (@Homepage, @RegistrationPage, @DashboardPage)

## Task One:

* Open Jisc_Automation_Test_Analyst project.
* Open Features folder.
* Open TaskOne.feature file.
* All the acceptance criteria for the three scenaios are mentioned.
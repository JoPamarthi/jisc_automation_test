@RegistrationTest 
@RegistrationPage

Feature: RegistrationPage displays users registration fields

  Scenario Outline: RegistrationPage should successfully display user registration fields
  
    Given I will navigate to registration page
    When I enter "<Name>" "<Email>" "<URL>" "<pass>" "<confirmpass>"
    Then I will click on Register
    Then I will navigate to dashboard page

    Examples: 
      | Name            | Email                     | URL                           | pass        | confirmpass |
      | Jisc Automation | AutomationTask@jisc.co.uk | https://www.automation.co.uk/ | Welcone@123 | Welcome@123 |

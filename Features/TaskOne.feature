@AcceptanceCriteria

Feature:

Scenario 1: Successfully Open the website and Navigate to Register page from Home Page

Given I open the website 
Then I should see the Home page with Button that Navigate to RegisterPage
And I click the button 
Then I should be able to navigate to Register Page 


Scenario 2 : Successfully enter all the fields in Register Page and validate any Invalid data in the fields

Given I an in the register page 
And Enter First Name
Then Validate First Name is successfully entered without Error
And Enter Email
Then Validate valid Email is Successfully Entered without Error
And Enter Password and Confirm Password
Then Validate if Password  is matching in both field and password enter is sucessful
And Enter Additional Notes
Then validate Additional Notes is entered Successfully

Scenario 3: Successfully submit resgister form and verify the Job Notifications in Dashboard Page

Given I enter all the details in the register form
And I click on Submit 
Then verify I was successfully navigated to Dashboard Page 
And Verify Job Notifications are present in Dashboard page


#Test Design Technique:
#Here I am using USE CASE test Design which  is a functional black box testing technique that helps to identify test scenarios that exercise the whole system on each transaction basis from start to finish.
@RegistrationTest 
@HomePage

Feature: Homepage should display Page title and Registration link

  Scenario: Homepage should successfully display Page title and Registration link
  
    Given I will navigate to HomePage 
    When I click on register button
    Then I can see registration page displayed
